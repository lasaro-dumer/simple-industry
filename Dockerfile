FROM node:latest
ARG DB_ACCOUNT_JSON=/dev/null
ARG ENV_FILE_PATH=.env
WORKDIR /app
COPY src .
COPY package.json .
COPY ${ENV_FILE_PATH} .env
COPY ${DB_ACCOUNT_JSON} db_account_key.json
RUN yarn install --production
CMD ["npm", "start"]