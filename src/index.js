require('dotenv').config();
const Commando = require('discord.js-commando');
const express = require('express');
const got = require('got');
const path = require('path');
const ServerInfoWrapper = require('./services/serverInfoWrapper');
const app = express();
const PORT = process.env.PORT || 5000;
const client = new Commando.Client({
	owner: '648027842008973312',
});

const OrderRepository = require('./db/order-repo');

client.registry
	// Registers your custom command groups
	.registerGroups([
		['market', 'Market commands'],
		['industry', 'Industry commands'],
	])
	// Registers all built-in groups, commands, and argument types
	.registerDefaults()
	// Registers all of your commands in the ./commands/ directory
	.registerCommandsIn(path.join(__dirname, 'commands'));

// fs.readdir(`${__dirname}/events/`, (err, files) => {
// 	files.forEach((file) => {
// 		const eventHandler = require(`${__dirname}/events/${file}`);
// 		const eventName = file.split('.')[0];
// 		client.on(eventName, (...args) => eventHandler(client, ...args));
// 	});
// });

client.login(process.env.BOT_TOKEN);

app.get('/', (req, res) => {
	console.log('Receiving request.');
	res.send('Hello World');
});

const keepAlivePath = 'keep-alive';

exports.getServerInfoWrapper = (serverId) => {
	return new ServerInfoWrapper(client, serverId);
};

app.get(`/${keepAlivePath}`, (req, res) => {
	console.log('Receiving request.');
	res.send('I\'m alive!');
});

app.get('/api/:server/orders/:status', async (req, res) => {
	const orderRepo = new OrderRepository();
	const openOrdersList = await orderRepo.getOrders(req.params.server, req.params.status);

	res.send(JSON.stringify(openOrdersList));
});

app.listen(PORT);

function keepAlive(nextTimeMinutes = process.env.KEEP_ALIVE_TIME) {
	setInterval(async () => {
		console.log('Starting keep alive request');
		const response = await got(`${process.env.HOST_URL}/${keepAlivePath}`);
		console.log(response.body);
	}, nextTimeMinutes * 60 * 1000);
}

console.log(`Listening on port ${PORT}`);

keepAlive();