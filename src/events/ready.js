require('dotenv').config();

module.exports = (client) => {
	console.log(`[${process.env.NAME}] Logged in as ${client.user.tag}!`);
};
