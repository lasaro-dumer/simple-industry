const { prefix } = require('../config.json');
const ServerInfoWrapper = require('./../services/serverInfoWrapper');

module.exports = async (client, message) => {
	if (!message.content.startsWith(prefix) || message.author.bot) return;

	const args = message.content.slice(prefix.length).trim().split(/ +/);
	const commandName = args.shift().toLowerCase();

	const command = client.commands.get(commandName)
		|| client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

	if (!command) {
		return message.reply('that\'s not a valid command!');
	}

	if (command.args && !args.length) {
		let reply = `You didn't provide any arguments, ${message.author}!`;

		if (command.usage) {
			reply += `\nThe proper usage would be: \`${prefix}${command.name} ${command.usage}\``;
		}

		return message.channel.send(reply);
	}

	let inServerMessage = false;
	let serverInfoWrapper = undefined;

	if (message.guild) {
		inServerMessage = true;
		serverInfoWrapper = new ServerInfoWrapper(client, message.guild.id, message);
	}

	if (command.onlyServer && !inServerMessage) {
		return message.channel.send('Sorry, this command in only valid on a server');
	}

	if (command.roles) {
		// let hasRole = false;
		// let x = _.intersection(command.roles, message.member.roles);
		// let v = command.roles.filter(value => message.member.roles.includes(value));
		const canRunCommand = command.roles.every(role => {
			return hasRole(message.member, role);
		});

		if (!canRunCommand) {
			return message.reply('sorry, but you can\'t run this command.');
		}
	}

	try {
		command.execute(message, args, serverInfoWrapper);
	}
	catch (error) {
		console.error(error);
		message.reply('there was an error trying to execute that command!');
	}
};
function hasRole(member, desiredRole) {
	return member.roles._roles.some(role => {
		if (role.name === desiredRole) {
			return true;
		}
	});
}