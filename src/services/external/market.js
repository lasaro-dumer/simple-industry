const fs = require('fs');
const got = require('got');

const rawdata = fs.readFileSync(`${__dirname}/../../data/items.json`);
const itemsData = JSON.parse(rawdata);
const itemsByName = [];
const itemsById = [];

itemsData.forEach(itemData => {
	itemsByName[itemData.name.toUpperCase()] = itemData.itemId;
	itemsById[itemData.itemId] = itemData.name;
});

module.exports = {
	getItemId(itemName) {
		return itemsByName[itemName.toUpperCase()];
	},
	async getLatestInfo(itemId) {
		try {
			/*
			time 		- a UNIX timestamp (seconds)
			sell 		- a calculated sell price for the item
			buy 		- a calculated buy price for the item
			lowest_sell - the lowest sell price for the item
			highest_buy - the highest buy price for the item
			volume 		- a predicted volume for the item
			*/
			const response = await got(`https://api.eve-echoes-market.com/market-stats/${itemId}`);

			const d = JSON.parse(response.body);
			const lastLine = d[d.length - 1];

			return {
				name: itemsById[itemId],
				sell: lastLine.sell,
				buy: lastLine.buy,
				lowestSell: lastLine.lowest_sell,
				highestBuy: lastLine.highest_buy,
				date: new Date(lastLine.time * 1000),
			};
		}
		catch (err) {
			console.log(`ID:${itemId}\nName:${itemsById[itemId]}\n${err.response.body}`);
			return undefined;
		}
	},
};