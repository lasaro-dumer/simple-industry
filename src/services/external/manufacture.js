const fs = require('fs');
const market = require('./market');
const rawdata = fs.readFileSync(`${__dirname}/../../data/ships.json`);
const shipsData = JSON.parse(rawdata);
const shipsByName = [];

shipsData.forEach(shipData => {
	shipsByName[shipData.name.toUpperCase()] = shipData;
});

async function addManufactureMaterial(name, quantity, efficiency) {
	if (quantity > 0) {
		const itemId = market.getItemId(name);
		if (itemId) {
			const mInfo = await market.getLatestInfo(itemId);

			const materialInfo = {
				name: name,
				unitaryPrice: mInfo.buy,
				quantity: calculateMaterialEfficiency(quantity, efficiency),
			};
			materialInfo.totalPrice = Math.ceil(materialInfo.unitaryPrice * materialInfo.quantity);

			return materialInfo;
		}
		else {
			console.log(`Unable to get item ID for ${name}`);
		}
	}

	return undefined;
}

function calculateMaterialEfficiency(quantity, efficiency) {
	return Math.ceil(quantity * efficiency);
}

module.exports = {
	async getManufactureCosts(shipName, materialEfficiency = 1, timeEfficiency = 1) {
		try {
			const shipData = shipsByName[shipName.toUpperCase()];

			if (shipData) {
				const manufactureCosts = {
					buildCost: shipData.manufactureCosts.ISK,
					buildTime: shipData.manufactureCosts.time * timeEfficiency,
					minerals: [
						await addManufactureMaterial('Tritanium', shipData.manufactureCosts.tritanium, materialEfficiency),
						await addManufactureMaterial('Pyerite', shipData.manufactureCosts.pyerite, materialEfficiency),
						await addManufactureMaterial('Mexallon', shipData.manufactureCosts.mexallon, materialEfficiency),
						await addManufactureMaterial('Isogen', shipData.manufactureCosts.isogen, materialEfficiency),
						await addManufactureMaterial('Nocxium', shipData.manufactureCosts.nocxium, materialEfficiency),
						await addManufactureMaterial('Zydrine', shipData.manufactureCosts.zydrine, materialEfficiency),
						await addManufactureMaterial('Megacyte', shipData.manufactureCosts.megacyte, materialEfficiency),
						await addManufactureMaterial('Morphite', shipData.manufactureCosts.morphite, materialEfficiency),
					],
					planetaryResources: [
						await addManufactureMaterial('Lustering Alloy', shipData.manufactureCosts.lusteringAlloy, materialEfficiency),
						await addManufactureMaterial('Sheen Compound', shipData.manufactureCosts.sheenCompound, materialEfficiency),
						await addManufactureMaterial('Gleaming Alloy', shipData.manufactureCosts.gleamingAlloy, materialEfficiency),
						await addManufactureMaterial('Condensed Alloy', shipData.manufactureCosts.condensedAlloy, materialEfficiency),
						await addManufactureMaterial('Precious Alloy', shipData.manufactureCosts.preciousAlloy, materialEfficiency),
						await addManufactureMaterial('Motley Compound', shipData.manufactureCosts.motleyCompound, materialEfficiency),
						await addManufactureMaterial('Fiber Composite', shipData.manufactureCosts.fiberComposite, materialEfficiency),
						await addManufactureMaterial('Lucent Compound', shipData.manufactureCosts.lucentCompound, materialEfficiency),
						await addManufactureMaterial('Opulent Compound', shipData.manufactureCosts.opulentCompound, materialEfficiency),
						await addManufactureMaterial('Glossy Compound', shipData.manufactureCosts.glossyCompound, materialEfficiency),
						await addManufactureMaterial('Crystal Compound', shipData.manufactureCosts.crystalCompound, materialEfficiency),
						await addManufactureMaterial('Dark Compound', shipData.manufactureCosts.darkCompound, materialEfficiency),
						await addManufactureMaterial('Reactive Gas', shipData.manufactureCosts.reactiveGas, materialEfficiency),
						await addManufactureMaterial('Noble Gas', shipData.manufactureCosts.nobleGas, materialEfficiency),
						await addManufactureMaterial('Base Metals', shipData.manufactureCosts.baseMetals, materialEfficiency),
						await addManufactureMaterial('Heavy Metals', shipData.manufactureCosts.heavyMetals, materialEfficiency),
						await addManufactureMaterial('Noble Metals', shipData.manufactureCosts.nobleMetals, materialEfficiency),
						await addManufactureMaterial('Reactive Metals', shipData.manufactureCosts.reactiveMetals, materialEfficiency),
						await addManufactureMaterial('Toxic Metals', shipData.manufactureCosts.toxicMetals, materialEfficiency),
						await addManufactureMaterial('Industrial Fibers', shipData.manufactureCosts.industrialFibers, materialEfficiency),
						await addManufactureMaterial('Supertensile Plastics', shipData.manufactureCosts.supertensilePlastics, materialEfficiency),
						await addManufactureMaterial('Polyaramids', shipData.manufactureCosts.polyaramids, materialEfficiency),
						await addManufactureMaterial('Coolant', shipData.manufactureCosts.coolant, materialEfficiency),
						await addManufactureMaterial('Condensates', shipData.manufactureCosts.condensates, materialEfficiency),
						await addManufactureMaterial('Construction Blocks', shipData.manufactureCosts.constructionBlocks, materialEfficiency),
						await addManufactureMaterial('Nanites', shipData.manufactureCosts.nanites, materialEfficiency),
						await addManufactureMaterial('Silicate Glass', shipData.manufactureCosts.silicateGlass, materialEfficiency),
						await addManufactureMaterial('Smartfab Units', shipData.manufactureCosts.smartfabUnits, materialEfficiency),
						await addManufactureMaterial('Heavy Water', shipData.manufactureCosts.heavyWater, materialEfficiency),
						await addManufactureMaterial('Suspended Plasma', shipData.manufactureCosts.suspendedPlasma, materialEfficiency),
						await addManufactureMaterial('Liquid Ozone', shipData.manufactureCosts.liquidOzone, materialEfficiency),
						await addManufactureMaterial('Ionic Solutions', shipData.manufactureCosts.ionicSolutions, materialEfficiency),
						await addManufactureMaterial('Oxygen Isotopes', shipData.manufactureCosts.oxygenIsotopes, materialEfficiency),
						await addManufactureMaterial('Plasmoids', shipData.manufactureCosts.plasmoids, materialEfficiency),
					],
				};

				return manufactureCosts;
			}
		}
		catch (err) {
			console.log(err);
		}

		return undefined;
	},
};