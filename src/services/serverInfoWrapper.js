class ServerInfoWrapper {
	constructor(client, serverId) {
		this.client = client;
		this.guild = client.guilds.resolve(serverId);
	}

	nickname(memberId = undefined) {
		if (memberId) {
			const member = this.guild.members.resolve(memberId);
			return member ? member.displayName : null;
		}

		if (this.message) {
			const member = this.guild.member(this.message.author);
			return member ? member.displayName : null;
		}

		return undefined;
	}

	tag(memberId = undefined) {
		if (memberId) {
			const member = this.guild.members.resolve(memberId);
			return member ? member.user.tag : null;
		}

		if (this.message) {
			const member = this.guild.member(this.message.author);
			return member ? member.displayName : null;
		}

		return undefined;
	}
}

module.exports = ServerInfoWrapper;