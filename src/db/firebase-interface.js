require('dotenv').config();

let dbAccountJsonPath = '../db_account_key.json';

if (process.env.DB_ACCOUNT_JSON_PATH) {
	dbAccountJsonPath = process.env.DB_ACCOUNT_JSON_PATH;
}

const admin = require('firebase-admin');
const serviceAccount = require(dbAccountJsonPath);

console.log(`Initializing DB using file from ${dbAccountJsonPath}`);

admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
});

console.log('Initialized DB');

const db = admin.firestore();

/*
function createUser() {
	const docRef = db.collection('users').doc('alovelace');
	const setAda = docRef.set({
		first: 'Ada',
		last: 'Lovelace',
		born: 1815,
	});
	return setAda;
}

function getUsers() {
	db.collection('users').get()
		.then((snapshot) => {
			snapshot.forEach((doc) => {
				console.log(doc.id, '=>', doc.data());
			});
		})
		.catch((err) => {
			console.log('Error getting documents', err);
		});
}
*/

exports.getDbConnection = () => {
	console.log('Retrieved DB instance');
	return db;
};
