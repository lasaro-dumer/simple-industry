const fbi = require('./firebase-interface');
const db = fbi.getDbConnection();

class OrderRepository {
	async getOrders(serverId, status = 'open') {
		const openOrdersList = [];
		const snapshot = await db.collection('orders')
			.where('serverId', '==', serverId)
			.where('status', '==', status).get();

		snapshot.forEach((doc) => {
			openOrdersList.push(doc.data());
		});

		return openOrdersList;
	}

	async getOrder(orderNumber) {
		const orderDoc = await db.collection('orders').doc(`${orderNumber}`).get();
		if (!orderDoc.exists) {
			return undefined;
		}
		else {
			return orderDoc.data();
		}
	}

	async assignOrder(serverId, vendorId, orderNumber) {
		const orderRef = db.collection('orders').doc(`${orderNumber}`);
		let orderDoc = await orderRef.get();
		if (!orderDoc.exists) {
			return undefined;
		}
		else {
			orderDoc = await orderRef.update({
				vendorId: vendorId,
				status: 'assigned',
				lastUpdate: new Date(),
			});
			return orderDoc;
		}
	}

	async createOrder(serverId, requesterId, itemName, itemQuantity) {
		const ordersRef = db.collection('orders');
		const snapshot = await ordersRef.orderBy('number', 'desc').limit(1).get();
		let lastOrderNumber = 0;

		if (snapshot.size > 0) {
			lastOrderNumber = snapshot.docs[0].data().number;
		}

		const nextNumber = lastOrderNumber + 1;

		const newOrder = {
			number: nextNumber,
			serverId: serverId,
			requesterId: requesterId,
			itemName: itemName,
			itemQuantity: itemQuantity,
			status: 'open',
			lastUpdate: new Date(),
		};

		ordersRef.doc(`${nextNumber}`).set(newOrder);

		return newOrder;
	}
}

module.exports = OrderRepository;