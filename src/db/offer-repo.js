const fbi = require('./firebase-interface');
const db = fbi.getDbConnection();

class OfferRepository {
	async getOffers(serverId, status = 'open') {
		const offersList = [];
		// This query requires an index, it will fail and suggest the index in the logs
		const snapshot = await db.collection('offers')
			.where('serverId', '==', serverId)
			.where('status', '==', status).get();

		snapshot.forEach((doc) => {
			offersList.push(doc.data());
		});

		return offersList;
	}

	async getOffer(serverId, offerNumber) {
		const offerDoc = await db.collection('offers').doc(offerNumber).get();
		if (!offerDoc.exists) {
			return undefined;
		}
		else {
			return offerDoc.data();
		}
	}

	async acceptOffer(serverId, buyerId, offerNumber) {
		const offerRef = db.collection('offers').doc(offerNumber);
		let offerDoc = await offerRef.get();
		if (!offerDoc.exists) {
			return undefined;
		}
		else {
			offerDoc = await offerRef.update({
				buyerId: buyerId,
				status: 'accepted',
				lastUpdate: new Date(),
			});
			return offerDoc;
		}
	}

	async createOffer(serverId, vendorId, itemName, itemQuantity, itemValue) {
		const offersRef = db.collection('offers');
		const snapshot = await offersRef.orderBy('number', 'desc').limit(1).get();
		let lastOffersNumber = 0;

		if (snapshot.size > 0) {
			lastOffersNumber = snapshot.docs[0].data().number;
		}

		const nextNumber = lastOffersNumber + 1;

		const newOffer = {
			number: nextNumber,
			serverId: serverId,
			vendorId: vendorId,
			itemName: itemName,
			itemQuantity: itemQuantity,
			itemValue: itemValue,
			status: 'open',
			lastUpdate: new Date(),
		};

		offersRef.doc(`${nextNumber}`).set(newOffer);

		return newOffer;
	}
}

module.exports = OfferRepository;
