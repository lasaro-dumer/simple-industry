const OrderRepository = require('../../db/order-repo');

const Commando = require('discord.js-commando');
const oneLine = require('common-tags').oneLine;

module.exports = class AssignOrderCommand extends Commando.Command {
	constructor(client) {
		super(client, {
			name: 'assign',
			aliases: [],
			group: 'market',
			memberName: 'assign',
			guildOnly: true,
			description: 'Assign the specified order to the user executing the command',
			details: oneLine`
				Will assign an order to you, the order state must be 'open' and will be 'assigned' after the command.
			`,
			examples: ['assign 12'],

			args: [
				{
					key: 'orderNumber',
					label: 'Order Number',
					prompt: 'What is the order number to be assigned?',
					type: 'integer',
				},
			],
		});
	}

	async run(msg, args) {
		const orderRepo = new OrderRepository();
		const orderNumber = args.orderNumber;
		const orderToAssign = await orderRepo.getOrder(orderNumber);

		// const id = args[1].match(/\d+/)[0];
		// message.serverInfoWrapper.guild.members.resolve(args[1].match(/\d+/)[0])

		if (orderToAssign && orderToAssign.status === 'open') {
			let assignedOrder = undefined;

			try {
				assignedOrder = await orderRepo.assignOrder(
					msg.guild.id,
					msg.author.id,
					orderNumber);
			}
			catch (error) {
				console.error(error);
			}

			if (assignedOrder) {
				msg.reply(`\nOrder #${orderNumber} assigned to you.`);
				// msg.channel.send(`<@${id}>`);
			}
			else {
				msg.reply(`\nSorry, error assigning order #${orderNumber} to you.`);
			}
		}
		else {
			msg.reply(`\nSorry, order #${orderNumber} not found or not open.`);
		}
	}
};
