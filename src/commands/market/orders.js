const OrderRepository = require('../../db/order-repo');
const cTable = require('console.table');
const Commando = require('discord.js-commando');
const oneLine = require('common-tags').oneLine;
const index = require('../../index');

module.exports = class ListOrdersCommand extends Commando.Command {
	constructor(client) {
		super(client, {
			name: 'orders',
			aliases: ['ordens', 'pedidos'],
			group: 'market',
			memberName: 'orders',
			guildOnly: true,
			description: 'List the orders.',
			details: oneLine`
				List the orders in a specif state, by default the state queried is 'open'.
			`,
			examples: ['orders assigned'],

			args: [
				{
					key: 'orderStatus',
					label: 'Order Status',
					prompt: 'What is the order status?',
					type: 'string',
					default: 'open',
				},
			],
		});
	}

	async run(msg, args) {
		const serverInfoWrapper = index.getServerInfoWrapper(msg.guild.id);
		const orderRepo = new OrderRepository();
		const openOrdersList = await orderRepo.getOrders(msg.guild.id, args.orderStatus);

		if (openOrdersList.length && openOrdersList.length > 0) {
			const tableItems = [];

			openOrdersList.forEach(order => {
				const lastUpdateDate = order.lastUpdate.toDate();
				let lastUpdateDateAsString = `${lastUpdateDate.toLocaleDateString()} ${lastUpdateDate.toTimeString()}`;
				lastUpdateDateAsString = lastUpdateDateAsString.substring(0, 19);

				tableItems.push({
					'#': order.number,
					Item: order.itemName,
					Quantity: order.itemQuantity,
					Requester: serverInfoWrapper.nickname(order.requesterId),
					Status: order.status,
					LastUpdate: lastUpdateDateAsString,
				});
			});

			const table = cTable.getTable(tableItems);

			msg.channel.send(`**Orders ${args.orderStatus}\n**\`\`\`\n${table}\`\`\``);
		}
		else {
			msg.channel.send(`**No ${args.orderStatus} Orders!**`);
		}
	}
};
