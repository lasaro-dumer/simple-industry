const Discord = require('discord.js');
const OrderRepository = require('../../db/order-repo');
const Commando = require('discord.js-commando');
const oneLine = require('common-tags').oneLine;

module.exports = class CreateBuyOrderCommand extends Commando.Command {
	constructor(client) {
		super(client, {
			name: 'buy',
			aliases: ['compro', 'wtb'],
			group: 'market',
			memberName: 'buy',
			guildOnly: true,
			description: 'Place a buying order for the desired item',
			details: oneLine`
				The order will be created as 'Open', you won't be able to set a price to your order.
				The price will be negotiated with the vendor.
			`,
			examples: ['buy Vexor 1'],

			args: [
				{
					key: 'itemName',
					label: 'Item Name',
					prompt: 'What is the item name?',
					type: 'string',
				},
				{
					key: 'quantity',
					label: 'Item Quantity',
					prompt: 'What is the quantity you want?',
					type: 'integer',
					default: 1,
				},
			],
		});
	}

	async run(msg, args) {
		const orderRepo = new OrderRepository();

		const createdOrder = await orderRepo.createOrder(
			msg.guild.id,
			msg.author.id,
			args.itemName,
			args.quantity);

		const embedMessage = new Discord.MessageEmbed();
		embedMessage.setTitle('Order Created');

		embedMessage.addFields(
			{ name: '#', value: createdOrder.number, inline: true },
			{ name: 'Item', value: createdOrder.itemName, inline: true },
			{ name: 'Quantity', value: createdOrder.itemQuantity, inline: true },
		);

		return msg.channel.send(embedMessage);
	}
};
