const OfferRepository = require('../../db/offer-repo');
const cTable = require('console.table');
const Commando = require('discord.js-commando');
const oneLine = require('common-tags').oneLine;
const index = require('../../index');

module.exports = class ListOffersCommand extends Commando.Command {
	constructor(client) {
		super(client, {
			name: 'offers',
			aliases: ['ofertas'],
			group: 'market',
			memberName: 'offers',
			guildOnly: true,
			description: 'List the offers.',
			details: oneLine`
				List the offers in a specif state, by default the state queried is 'open'.
			`,
			examples: ['offers sold'],

			args: [
				{
					key: 'offerStatus',
					label: 'Offer Status',
					prompt: 'What is the offer status?',
					type: 'string',
					default: 'open',
				},
			],
		});
	}

	async run(msg, args) {
		// module.exports = {
		// 	name: 'offers',
		// 	description: 'List the offers.',
		// 	args: false,
		// 	aliases: ['ordens', 'pedidos'],
		// 	// usage: '<user> <role>',
		// 	onlyServer: true,
		// 	// cooldown: 5,
		// 	async execute(message, args, serverInfoWrapper) {
		const serverInfoWrapper = index.getServerInfoWrapper(msg.guild.id);
		const offerRepo = new OfferRepository();

		// const embedMessage = new Discord.MessageEmbed();
		// embedMessage.setTitle('Orders');

		const openOffersList = await offerRepo.getOffers(msg.guild.id, args.orderStatus);

		if (openOffersList.length && openOffersList.length > 0) {
			const tableItems = [];

			openOffersList.forEach(offer => {
				const lastUpdateDate = offer.lastUpdate.toDate();
				let lastUpdateDateAsString = `${lastUpdateDate.toLocaleDateString()} ${lastUpdateDate.toTimeString()}`;
				lastUpdateDateAsString = lastUpdateDateAsString.substring(0, 19);

				tableItems.push({
					'#': offer.number,
					Item: offer.itemName,
					Quantity: offer.itemQuantity,
					Value: offer.itemValue,
					Vendor: serverInfoWrapper.nickname(offer.vendorId),
					Status: offer.status,
					LastUpdate: lastUpdateDateAsString,
				});
			});

			const table = cTable.getTable(tableItems);

			msg.channel.send(`**Open Offers\n**\`\`\`\n${table}\`\`\``);
		}
		else {
			msg.channel.send('**No open Offers!**');
		}
	}
};