const Discord = require('discord.js');
const OfferRepository = require('../../db/offer-repo');
const Commando = require('discord.js-commando');
const oneLine = require('common-tags').oneLine;
const index = require('../../index');

module.exports = class CreateSellOfferCommand extends Commando.Command {
	constructor(client) {
		super(client, {
			name: 'sell',
			aliases: ['vendo', 'wts'],
			group: 'market',
			memberName: 'sell',
			guildOnly: true,
			description: 'Place a selling offer',
			details: oneLine`
				The order will be created as 'Open', you won't be able to set a price to your order.
				The price will be negotiated with the vendor.
			`,
			examples: ['sell Vexor 40kk 1'],

			args: [
				{
					key: 'itemName',
					label: 'Item Name',
					prompt: 'What is the item name?',
					type: 'string',
				},
				{
					key: 'value',
					label: 'ISK Value',
					prompt: 'What is the ISK value of the offer?',
					type: 'float',
					default: 1,
				},
				{
					key: 'quantity',
					label: 'Item Quantity',
					prompt: 'What is the quantity you are selling?',
					type: 'integer',
					default: 1,
				},
			],
		});
	}

	async run(msg, args) {
		console.log('args', args);
		const offerRepo = new OfferRepository();
		const serverInfoWrapper = index.getServerInfoWrapper(msg.guild.id);

		const createdOffer = await offerRepo.createOffer(
			msg.guild.id,
			msg.author.id,
			args.itemName,
			args.quantity,
			args.value);

		const embedMessage = new Discord.MessageEmbed();
		embedMessage.setTitle('Offer Created');

		embedMessage.addFields(
			{ name: '#', value: createdOffer.number, inline: true },
			{ name: 'Requester Nick', value: serverInfoWrapper.nickname(msg.author.id), inline: true },
			{ name: 'Requester Tag', value: serverInfoWrapper.tag(msg.author.id), inline: true },
		);

		return msg.channel.send(embedMessage);
	}
};
