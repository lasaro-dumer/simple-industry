// const market = require('../services/external/market');

// module.exports = {
// 	name: 'price',
// 	description: 'Provides the price/market info of an item',
// 	args: true,
// 	aliases: ['preco'],
// 	usage: '<item name>',
// 	// cooldown: 5,
// 	async execute(message, args) {
// 		const itemName = args[0];
// 		const itemId = market.getItemId(itemName);

// 		if (itemId) {
// 			const mInfo = await market.getLatestInfo(itemId);

// 			if (mInfo) {
// 				message.channel.send(`Received data:\`\`\`\nSell:${mInfo.sell}\nBuy: ${mInfo.buy}\nDate:${mInfo.date.toLocaleString()}\`\`\``);
// 			}
// 			else {
// 				message.channel.send('Unable to get latest market information.');
// 			}
// 		}
// 		else {
// 			message.channel.send('Item not found!');
// 		}
// 	},
// };
