// /* eslint-disable no-inline-comments */
// const Discord = require('discord.js');
// const manufacture = require('../services/external/manufacture');
// const market = require('../services/external/market');

// module.exports = {
// 	name: 'bp',
// 	description: 'Provides the manufacture info of a ship (only ships for now)',
// 	args: true,
// 	// aliases: ['preco'],
// 	usage: '<ship name> [material efficiency %] [time efficiency %]',
// 	// cooldown: 5,
// 	roles: ['industrialist'],
// 	async execute(message, args) {
// 		const shipName = args[0];
// 		const materialEfficiencyArg = args.length > 1 ? args[1] : 100;
// 		const timeEfficiencyArg = args.length > 1 ? args[2] : 100;
// 		const materialEfficiency = materialEfficiencyArg / 100;
// 		const timeEfficiency = timeEfficiencyArg / 100;

// 		const shipManufactureCosts = await manufacture.getManufactureCosts(shipName, materialEfficiency, timeEfficiency);

// 		if (shipManufactureCosts) {
// 			let totalCost = 0;
// 			totalCost += shipManufactureCosts.buildCost;

// 			const embedMessage = new Discord.MessageEmbed();
// 			embedMessage.setTitle(shipName);

// 			embedMessage.addField('Manufacture Cost', shipManufactureCosts.buildCost, true);

// 			const date = new Date(0);
// 			date.setSeconds(shipManufactureCosts.buildTime); // specify value for SECONDS here
// 			const timeString = date.toISOString().substr(11, 8);

// 			embedMessage.addField('Manufacture Time', timeString, true);

// 			const blueprintItemId = market.getItemId(`${shipName} blueprint`);
// 			const blueprintMarketInfo = await market.getLatestInfo(blueprintItemId);
// 			embedMessage.addField('Blueprint Cost', blueprintMarketInfo.buy, true);
// 			totalCost += blueprintMarketInfo.buy;

// 			let mineralsMessage = '';
// 			let mineralsTotalISK = 0;

// 			shipManufactureCosts.minerals.forEach(mineral => {
// 				if (mineral && mineral.quantity > 0) {
// 					mineralsMessage += `${mineral.name.padEnd(13)}${mineral.quantity} [${mineral.unitaryPrice} ISK ∏ ${mineral.totalPrice}]\n`;
// 					mineralsTotalISK += mineral.totalPrice;
// 				}
// 			});

// 			mineralsMessage += `\nTotal: ${mineralsTotalISK}\n`;

// 			embedMessage.addField('Minerals', `\`\`\`${mineralsMessage}\`\`\``, false);

// 			let planetaryMessage = '';
// 			let planetaryTotalISK = 0;

// 			shipManufactureCosts.planetaryResources.forEach(planetaryResource => {
// 				if (planetaryResource && planetaryResource.quantity > 0) {
// 					planetaryMessage += `${planetaryResource.name.padEnd(25)}${planetaryResource.quantity} [${planetaryResource.unitaryPrice} ISK ∏ ${planetaryResource.totalPrice}]\n`;
// 					planetaryTotalISK += planetaryResource.totalPrice;
// 				}
// 			});

// 			planetaryMessage += `\nTotal: ${planetaryTotalISK}\n`;

// 			embedMessage.addField('Planetary Resources', `\`\`\`${planetaryMessage}\`\`\``, false);

// 			totalCost += mineralsTotalISK;
// 			totalCost += planetaryTotalISK;

// 			embedMessage.addField('Total Cost', totalCost, true);

// 			// message.channel.send(embedMessage);
// 			message.author.send(embedMessage)
// 				.then(() => {
// 					if (message.channel.type === 'dm') return;
// 					message.reply('DM sent with results');
// 				})
// 				.catch(error => {
// 					console.error(`Could not send help DM to ${message.author.tag}.\n`, error);
// 					message.reply('it seems like I can\'t DM you! Do you have DMs disabled?');
// 				});
// 		}
// 		else {
// 			message.channel.send('Ship manufacture costs not found!');
// 		}
// 	},
// };
