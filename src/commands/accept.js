// const OfferRepository = require('../db/offer-repo');

// module.exports = {
// 	name: 'accept',
// 	description: 'Accept the specified offer',
// 	args: true,
// 	aliases: [],
// 	usage: '<offer number>',
// 	onlyServer: true,
// 	// cooldown: 5,
// 	async execute(message, args) {
// 		const offerRepo = new OfferRepository();
// 		const offerNumber = args[0];

// 		const offerToAccept = await offerRepo.getOffer(message.guild.id, offerNumber);

// 		if (offerToAccept && offerToAccept.status === 'open') {
// 			let assignedOffer = undefined;

// 			try {
// 				assignedOffer = await offerRepo.acceptOffer(
// 					message.guild.id,
// 					message.author.id,
// 					offerNumber);
// 			}
// 			catch (error) {
// 				console.error(error);
// 			}

// 			if (assignedOffer) {
// 				message.reply(`\nOffer #${offerNumber} assigned to you.`);
// 			}
// 			else {
// 				message.reply(`\nSorry, error assigning offer #${offerNumber} to you.`);
// 			}
// 		}
// 		else {
// 			message.reply(`\nSorry, offer #${offerNumber} not found or not open.`);
// 		}
// 	},
// };
